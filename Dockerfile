FROM openjdk:11-jre-slim

EXPOSE 8080

# The Spring Boot application's generated jar file
ARG JAR_ORIGIN=build/libs/transactions-service-0.0.1-SNAPSHOT.jar

# Add the jar file to the container
ADD ${JAR_ORIGIN} /opt/transactions-service/transactions-service-0.0.1-SNAPSHOT.jar

# Run the jar file
ENTRYPOINT ["java", "-Djava.security.egd=file:/dev/./urandom", "-jar", "/opt/transactions-service/transactions-service-0.0.1-SNAPSHOT.jar"]