package com.digibank.transactions.controller.v1.api;


import com.digibank.transactions.service.TransactionsService;
import com.digibank.transactions.dto.TransactionDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
public class TransactionController {

    @Autowired
    private TransactionsService transactionsService;

    @RequestMapping(value = "/v1/transactions/{accountNumber}", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    public ResponseEntity<List<TransactionDto>> findTransactionsByDate(
        @PathVariable("accountNumber")
        Integer accountNumber,
        @RequestParam(value = "from")
        String from,
        @RequestParam(value = "to", required = false)
        String to
    ) {
        log.info("Account number: {}", accountNumber);
        return ResponseEntity.ok(transactionsService.transactions(accountNumber, from, to));
    }

}
