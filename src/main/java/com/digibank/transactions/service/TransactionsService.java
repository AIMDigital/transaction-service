package com.digibank.transactions.service;

import com.digibank.transactions.dto.TransactionDto;
import org.springframework.lang.Nullable;

import java.util.List;

public interface TransactionsService {

    List<TransactionDto> transactions(Integer accountNumber, String from, @Nullable String to);
}