package com.digibank.transactions.service.impl;

import com.digibank.transactions.service.TransactionsService;
import com.github.javafaker.Faker;
import com.digibank.transactions.dto.TransactionDto;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@Component
public class TransactionsServiceImpl implements TransactionsService {

    @Override
    public List<TransactionDto> transactions(Integer accountNumber, String from, String to) {

        LocalDate fromDate = LocalDate.parse(from);
        LocalDate toDate = to != null ? LocalDate.parse(to) : LocalDate.now();

        int transactionsCount = 0;
        List<TransactionDto> transactions = new ArrayList<>();

        while (transactionsCount < 10) {

            Faker faker = new Faker();
            TransactionDto fakeTransaction = new TransactionDto(
                faker.number().randomNumber(8, true),
                new Date(faker.date().between(Date.valueOf(fromDate), Date.valueOf(toDate)).getTime()).toLocalDate(),
                faker.commerce().productName(),
                new BigDecimal(faker.commerce().price())
            );

            transactions.add(fakeTransaction);

            transactionsCount++;
        }

        transactions.sort(Comparator.comparing(TransactionDto::getDate));

        return transactions;

    }
}
