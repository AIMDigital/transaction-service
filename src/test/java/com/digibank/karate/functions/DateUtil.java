package com.digibank.karate.functions;

import java.time.LocalDate;

public class DateUtil {

    public boolean isEqualOrBefore(String date1, String date2) {

        LocalDate dateOne = LocalDate.parse(date1);
        LocalDate dateTwo = LocalDate.parse(date2);

        return dateOne.isEqual(dateTwo) || dateOne.isBefore(dateTwo);
    }

    public boolean isBeforeNow(String date) {
        return isEqualOrBefore(date, LocalDate.now().toString());
    }

    public boolean isEqualOrAfter(String date1, String date2) {

        LocalDate dateOne = LocalDate.parse(date1);
        LocalDate dateTwo = LocalDate.parse(date2);

        return dateOne.isEqual(dateTwo) || dateOne.isAfter(dateTwo);
    }

    public boolean isAfterNow(String date) {
        return isEqualOrAfter(date, LocalDate.now().toString());
    }

}