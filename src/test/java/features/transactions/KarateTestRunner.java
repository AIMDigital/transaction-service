package features.transactions;

import com.intuit.karate.junit5.Karate;

public class KarateTestRunner {

    @Karate.Test
    Karate tests(){
        return Karate.run("transactions").relativeTo(getClass());
    }

}