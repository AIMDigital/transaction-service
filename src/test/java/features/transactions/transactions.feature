Feature: Transactions Service REST API

  Background:
    * url 'http://localhost:8080'
    * header Accept = 'application/json'
    * def fromDate = '2020-01-01'
    * def toDate = '2020-01-31'
    * def isBefore =
    """
    function(date1, date2) {
      var DateUtil = Java.type('com.digibank.karate.functions.DateUtil');
      var dateUtil = new DateUtil();
      return dateUtil.isEqualOrBefore(date1, date2);
    }
    """
    * def isBeforeNow =
    """
    function(date) {
      var DateUtil = Java.type('com.digibank.karate.functions.DateUtil');
      var dateUtil = new DateUtil();
      return dateUtil.isBeforeNow(date);
    }
    """
    * def isAfter =
    """
    function(date1, date2) {
      var DateUtil = Java.type('com.digibank.karate.functions.DateUtil');
      var dateUtil = new DateUtil();
      return dateUtil.isEqualOrAfter(date1, date2);
    }
    """

  Scenario: Test valid GET endpoint
    Given path '/v1/transactions/123'
    And param from = fromDate
    When method GET
    Then status 200

  Scenario: Test expected fields and field formats in response
    Given path '/v1/transactions/123'
    And param from = fromDate
    When method GET
    Then match each response[*].id == '#number'
    Then match each response[*].date == '#? isBeforeNow(_)'
    Then match each response[*].date == '#? isAfter(_, fromDate)'
    Then match each response[*].description == '#string'
    Then match each response[*].amount == '#number'

    Given path '/v1/transactions/123'
    And param from = fromDate
    And param to = toDate
    When method GET
    Then match each response[*].id == '#number'
    Then match each response[*].date == '#? isBefore(_, toDate)'
    Then match each response[*].description == '#string'
    Then match each response[*].amount == '#number'